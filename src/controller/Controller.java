package controller;

import java.util.ArrayList;
import java.util.Scanner;

import model.data_structures.Cola;
import model.data_structures.Node;
import model.data_structures.Viaje;
import model.logic.MVCModelo;
import view.MVCView;

public class Controller {

	/* Instancia del Modelo*/
	private MVCModelo modelo;

	/* Instancia de la Vista*/
	private MVCView view;

	/**
	 * Crear la vista y el modelo del proyecto
	 * @param capacidad tamaNo inicial del arreglo
	 */
	public Controller ()
	{
		view = new MVCView();
		modelo = new MVCModelo();
	}

	public void run() 
	{
		Scanner lector = new Scanner(System.in);
		boolean fin = false;
		String dato = "";
		String respuesta = "";

		while( !fin ){
			view.printMenu();

			int option = lector.nextInt();
			switch(option){
			case 1:
				System.out.println("Ingrese el trimestre deseado: ");
				dato=lector.next();
				modelo.cargartrimestre(Integer.parseInt(dato));
				System.out.println("El total de viajes en el archivo de meses en el trimestre es: " + modelo.darTamanomes());
				System.out.println("El total de viajes en el archivo de d�as en el trimestre es: " + modelo.darTamanosemana());
				System.out.println("El total de viajes en el archivo de horas en el trimestre es: " + modelo.darTamanohoras());
				System.out.println("La zona con menor identificador en el trimestre es "+modelo.zonamenorident() );
				System.out.println("La zona con mayor identificador en el trimestre es "+modelo.zonamayorident()  );

				break;	

			case 2:
				String mes;
				String or;
				String des;
				System.out.println("Ingrese el mes deseado: ");
				mes=lector.next();
				System.out.println("Ingrese el origen: ");
				or=lector.next();
				System.out.println("Ingrese el destino: ");
				des=lector.next();
				if(modelo.consultaPorMesZona(Integer.parseInt(mes), Integer.parseInt(or), Integer.parseInt(des))==null)
					System.out.println("No existen viajes que cumplan con estos datos");
				else
				{
					System.out.println("Promedio y desviaci�n estandar de viajes que cumplen con los datos ingresados: ");
					Node nod=modelo.consultaPorMesZona(Integer.parseInt(mes), Integer.parseInt(or), Integer.parseInt(des)).darPrimer();
					while(nod!=null)
					{
						Viaje vi=(Viaje) nod.darDato();
						System.out.println("Promedio: "+vi.darMean_travel_time()+" - Desviaci�n est�ndar: "+vi.darStandard_deviation_travel_time());
						nod=nod.darSiguiente();
					}
				}

				break;
				
			case 3:
				System.out.println("Ingresar mes: ");
				String ms= lector.next();
				System.out.println("Ingresar n�mero de viajes: ");
				String num=lector.next();
				System.out.println("Viajes con m�s promedio:");
				Viaje[] viaj=modelo.viajesMasPromMes(Integer.parseInt(ms), Integer.parseInt(num));
				for(int i=0; i<viaj.length;i++)
				{
					System.out.println("Zona de origen: "+viaj[i].darSourceid()+" , Zona de destino: "+viaj[i].darDstid()+" , Tiempo promedio: "+viaj[i].darMean_travel_time()+" , Desviaci�n estandar: "+ viaj[i].darStandard_deviation_travel_time());
				}
				break;

			case 4: 
				
				System.out.println("Ingrese la zona de interes: ");
				int zon1 = Integer.parseInt(lector.next());
				System.out.println("Ingrese la zona menor: ");
				int zon2 = Integer.parseInt(lector.next());
				System.out.println("Ingrese la zona mayor: ");
				int zon3 = Integer.parseInt(lector.next());
				System.out.println("Ingrese el dia: ");
				int di = Integer.parseInt(lector.next());
				ArrayList<String> lista2 = modelo.comparacionZona(zon1, zon2, zon3, di);
				for(int i = 0; i<lista2.size();i++)
				{
					System.out.println(lista2.get(i));
				}

				break;	

			case 5:
				System.out.println("Ingrese el dia deseado: ");
				String dia=lector.next();
				System.out.println("Ingrese el origen: ");
				String origen=lector.next();
				System.out.println("Ingrese el destino: ");
				String destino=lector.next();
				if(modelo.consultarPorDiayZona(Integer.parseInt(dia), Integer.parseInt(origen), Integer.parseInt(destino)).estaVacia()==true)
				{
					System.out.println("No existen viajes con el origen y destino seleccionados en el dia deseado ");
				}
				else
				{
					System.out.println("Promedio y desviacion estandar de los viajes con el origen seleccionado en el dia deseado: ");
					Cola<Viaje> viajes = modelo.consultarPorDiayZona(Integer.parseInt(dia), Integer.parseInt(origen), Integer.parseInt(destino));
					Node<Viaje> nodo = viajes.darPrimer();
					while(nodo!=null)
					{
						System.out.println("Promedio de viaje: " + nodo.darDato().darMean_travel_time() + "  Desviacion estandar: " + nodo.darDato().darStandard_deviation_travel_time());
					}
				}
				
				break;
			case 6:
				
				System.out.println("Ingresar dia: ");
				String diaa= lector.next();
				System.out.println("Ingresar n�mero de viajes(N): ");
				String N=lector.next();
				System.out.println("Viajes con mayor promedio:");
				Viaje[] arreglo =modelo.consultarNViajesDia(Integer.parseInt(N), Integer.parseInt(diaa));
				for(int i=0; i<arreglo.length;i++)
				{
					System.out.println("Zona de origen: "+ arreglo[i].darSourceid()+" , Zona de destino: "+arreglo[i].darDstid()+" , Tiempo promedio: "+arreglo[i].darMean_travel_time()+" , Desviaci�n estandar: "+ arreglo[i].darStandard_deviation_travel_time());
				}
				
				break;
			case 7:
				
				System.out.println("Ingrese la zona de interes: ");
				int zonaInt = Integer.parseInt(lector.next());
				System.out.println("Ingrese la zona menor: ");
				int zonaMenor = Integer.parseInt(lector.next());
				System.out.println("Ingrese la zona mayor: ");
				int zonaMayor = Integer.parseInt(lector.next());
				System.out.println("Ingrese el dia: ");
				int diaUlt = Integer.parseInt(lector.next());
				ArrayList<String> lista = modelo.consultarPorZona(zonaInt, zonaMenor, zonaMayor, diaUlt);
				for(int i = 0; i<lista.size();i++)
				{
					System.out.println(lista.get(i));
				}
				break;
			case 8:
				System.out.println("Ingresar hora: ");
				String horaaa= lector.next();
				System.out.println("Ingresar n�mero de viajes: ");
				String fjff=lector.next();
				System.out.println("Viajes con m�s promedio:");
				Viaje[] viajeee=modelo.viajesMasPromHora(Integer.parseInt(horaaa), Integer.parseInt(fjff));
				for(int i=0; i<viajeee.length;i++)
				{
					System.out.println("Zona de origen: "+viajeee[i].darSourceid()+" , Zona de destino: "+viajeee[i].darDstid()+" , Tiempo promedio: "+viajeee[i].darMean_travel_time()+" , Desviaci�n estandar: "+ viajeee[i].darStandard_deviation_travel_time());
				}
				break;
			case 9:
				System.out.println("Ingresar la hora: ");
				String horaa= lector.next();
				System.out.println("Ingresar n�mero de viajes(N): ");
				String NN=lector.next();
				System.out.println("Viajes con mayor promedio:");
				Viaje[] ar =modelo.consultarNViajesDia(Integer.parseInt(horaa), Integer.parseInt(NN));
				for(int i=0; i<ar.length;i++)
				{
					System.out.println("Zona de origen: "+ ar[i].darSourceid()+" , Zona de destino: "+ar[i].darDstid()+" , Tiempo promedio: "+ar[i].darMean_travel_time()+" , Desviaci�n estandar: "+ ar[i].darStandard_deviation_travel_time());
				}
				break;
			case 10:
				System.out.println("Grafica ASCII");
				System.out.println("Ingrese el origen:");
				int org = Integer.parseInt(lector.next());
				System.out.println("Ingrese el destino:");
				int dest = Integer.parseInt(lector.next());
				modelo.graficaASCII(org, dest);
				
				break;
			case 11:
				System.out.println("--------- \n Hasta pronto !! \n---------"); 
				lector.close();
				fin = true;
				break;
			default: 
				System.out.println("--------- \n Opcion Invalida !! \n---------");
				break;
			}
		}

	}	
}
