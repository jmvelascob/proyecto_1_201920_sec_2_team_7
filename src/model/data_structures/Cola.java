package model.data_structures;


public class Cola <T extends Comparable<T>> implements ICola<T>
	{

		
		private Node<T> primer;
		private Node<T> ultimo;
		
		//Constructor de la lista
		public Cola()
		{
			primer = null;
			ultimo = null;
		}
		
		public boolean estaVacia()
		{
			if(primer==null)
			{
				return true;
			}
			return false;
		}
		
		public Node<T> darPrimer()
		{
			return primer;
		}
		
		public Node<T> darUltimo()
		{
			return ultimo;
		}
		
		public int darTamano()
		{
			if(estaVacia())
			{
				return 0;
			}
			else
			{
				Node<T> actual = primer;
				int cont = 0;
				while(actual != null)
				{
					cont++;
					actual = actual.darSiguiente();
				}
				return cont;
			}
		}

		public void enqueue(T dato) 
		{
			if(primer == null)
			{
				Node<T> pr = new Node<T>(dato);
				primer = pr;
				ultimo = pr;
			}
			else
			{
				Node<T> ult = ultimo;
				Node<T> nuevo = new Node<T>(dato);
				ult.cambiarSiguiente(nuevo);
				ultimo = nuevo;
			}	
		}


		public Node<T> dequeue() {
			// TODO Auto-generated method stub
			if(primer!=null)
			{
				Node<T> elimin = primer;
				primer = primer.darSiguiente();
				return elimin;
			}
			return null;		}


	}


