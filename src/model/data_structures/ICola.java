package model.data_structures;
import model.data_structures.Node;

public interface ICola <T>{


	
		//Retorna el primer dato
			public Node<T> darPrimer();
			
		//Retorna el ultimo dato
			public Node<T> darUltimo();
			
		//Retorna el tama�o
			public int darTamano();
			
		//Retorna si la cola esta vacia
			public boolean estaVacia();
			
		//Agrega un dato a la lista
			void enqueue(T dato);

		//Elimina un dato de la lista
			Node<T> dequeue();
	}

