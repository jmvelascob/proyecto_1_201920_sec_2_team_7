
	package model.data_structures;

	import java.io.Serializable;

	public class Node<T> 
	{
		 T dato;
		
		 Node<T> siguiente;
		
		public Node( T pDato)
		{
			siguiente = null;
			dato = pDato;
		}
		
		public Node<T> darSiguiente()
		{
			return siguiente;
		}
		
		public void cambiarSiguiente( Node<T> sigui)
		{
			siguiente = sigui;
		}
		
		public T darDato()
		{
			return dato;
		}
		
		public void cambiarDato(T pdato)
		{
			dato=pdato;
		}


}
