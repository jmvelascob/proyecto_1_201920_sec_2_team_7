package model.logic;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

import com.opencsv.CSVReader;

import model.data_structures.Cola;
import model.data_structures.Node;
import model.data_structures.Viaje;

public class MVCModelo {
	/**
	 * Atributos del modelo del mundo
	 */
	private Cola<Viaje> colames;
	private Cola<Viaje> colasemana;
	private Cola<Viaje> colahoras;
	int mayorsemana;
	int mayorhora;
	int menormes;
	int menorsemana;
	int menorhora;
	int mayormes;
	/**
	 * Constructor del modelo del mundo con capacidad predefinida
	 */
	public MVCModelo()
	{
		colames = new Cola<Viaje>();
		colasemana= new Cola <Viaje>();
		colahoras= new Cola<Viaje>();
		mayorsemana=0;
		mayorhora=0;
		menormes=1000000;
		menorsemana=1000000;
		menorhora=1000000;
		mayormes=0;
	}


	/**
	 * Servicio de consulta de numero de elementos presentes en el modelo 
	 * @return numero de elementos presentes en el modelo
	 */
	public int darTamanohoras()
	{
		return colahoras.darTamano();
	}
	public int darTamanosemana()
	{
		return colasemana.darTamano();
	}
	public int darTamanomes()
	{
		return colames.darTamano();
	}
	public Cola<Viaje> darColames()
	{
		return colames;
	}
	public Cola<Viaje> darColasemana()
	{
		return colasemana;
	}
	public void cargartrimestre(int trim)
	{
		CSVReader reader = null;
		CSVReader reader1=null;
		CSVReader reader2=null;

		
		try {

			reader = new CSVReader(new FileReader("./data/bogota-cadastral-2018-"+trim+"-All-HourlyAggregate.csv"));
			reader1 = new CSVReader(new FileReader("./data/bogota-cadastral-2018-"+trim+"-WeeklyAggregate.csv"));
			reader2= new CSVReader(new FileReader("./data/bogota-cadastral-2018-"+trim+"-All-MonthlyAggregate.csv"));

			try{
				String[] x =reader.readNext();
				String[] y =reader1.readNext();
				String[] z =reader2.readNext();
			}
			catch(Exception e)
			{
				System.out.println("Problemas leyendo la primera linea");
			}

			for(String[] nextLine : reader) 
			{
				Viaje dato = new Viaje( Integer.parseInt(nextLine[0]), Integer.parseInt(nextLine[1]), Integer.parseInt(nextLine[2]), Double.parseDouble(nextLine[3]), Double.parseDouble(nextLine[4]), Double.parseDouble(nextLine[5]), Double.parseDouble(nextLine[6]));
				
				if(dato.darDstid()>mayormes)
				{
					mayormes=dato.darDstid();
				}
				if(dato.darSourceid()>mayormes)
				{
					mayormes=dato.darSourceid();
				}
				if(dato.darDstid()<menormes)
				{
					menormes=dato.darDstid();
				}
				if(dato.darSourceid()<menormes)
				{
					menormes=dato.darSourceid();
				}
				
				colahoras.enqueue(dato);
				
			}

			for(String[] nextLine : reader1) 
			{
				Viaje dato = new Viaje( Integer.parseInt(nextLine[0]), Integer.parseInt(nextLine[1]), Integer.parseInt(nextLine[2]), Double.parseDouble(nextLine[3]), Double.parseDouble(nextLine[4]), Double.parseDouble(nextLine[5]), Double.parseDouble(nextLine[6]));
				colasemana.enqueue(dato);
				
				if(dato.darDstid()>mayorsemana)
				{
					mayorsemana=dato.darDstid();
				}
				if(dato.darSourceid()>mayorsemana)
				{
					mayorsemana=dato.darSourceid();
				}
				if(dato.darDstid()<menorsemana)
				{
					menorsemana=dato.darDstid();
				}
				if(dato.darSourceid()<menorsemana)
				{
					menorsemana=dato.darSourceid();
				}
			}

			for(String[] nextLine : reader2) 
			{
				Viaje dato = new Viaje( Integer.parseInt(nextLine[0]), Integer.parseInt(nextLine[1]), Integer.parseInt(nextLine[2]), Double.parseDouble(nextLine[3]), Double.parseDouble(nextLine[4]), Double.parseDouble(nextLine[5]), Double.parseDouble(nextLine[6]));
				colames.enqueue(dato);
				if(dato.darDstid()>mayorhora)
				{
					mayorhora=dato.darDstid();
				}
				if(dato.darSourceid()>mayorhora)
				{
					mayorhora=dato.darSourceid();
				}
				if(dato.darDstid()<menorhora)
				{
					menorhora=dato.darDstid();
				}
				if(dato.darSourceid()<menorhora)
				{
					menorhora=dato.darSourceid();
				}
			}
		}
		catch (FileNotFoundException e) 
		{
			e.printStackTrace();
		} 
		finally
		{
			if (reader != null && reader1!=null && reader2!=null) 
			{
				try 
				{
					reader.close();
					reader1.close();
					reader2.close();
				} catch (IOException e) 
				{
					e.printStackTrace();
				}
			}
		}
	}
	
	public int zonamenorident()
	{
		int menor=menormes;
		
		if(menorsemana<menor)
		{
			menor=menorsemana;
		}
		if(menorhora<menor)
		{
			menor=menorhora;
		}
		return menor;
	} 
	
	public Cola<Viaje> darColahoras()
	{
		return colahoras;
	}


	public int zonamayorident()
	{
		int mayor=mayormes;

		if(mayorsemana>mayor)
		{
			mayor=mayorsemana;
		}
		if(mayorhora>mayor)
		{
			mayor=mayorhora;
		}
		return mayor;
	}

	//En el controller llamo la desviaci�n y el promedio de cada viaje
	public Cola<Viaje> consultaPorMesZona(int mes, int origen, int destino)
	{
		Node viajenodo= colames.darPrimer();
		Cola<Viaje> viajes= new Cola<Viaje>();
		while(viajenodo!=null)
		{
			Viaje viajee=(Viaje) viajenodo.darDato();
			if(viajee.darInfo()==mes && viajee.darDstid()>=destino && viajee.darSourceid()<=origen)
			{
				viajes.enqueue(viajee);
			}
			viajenodo=viajenodo.darSiguiente();
		}
		if (viajes.darPrimer()==null)
			return null;
		else
			return viajes;
	}
	
	//ShellSort
	
	public Comparable[] shellsort(Comparable arr[])
	{
		int n=arr.length;
		int h=1;
		while(h<n/3)
		{
			h=3*h+1;
		}
		
		while(h>=1)
		{
			for(int i=h; i<n; i++)
			{
				Comparable temp=arr[i];
				
				for(int j=i; j>=h && arr[j].compareTo(arr[j-h])<0; j-=h )
				{
					arr[j]=arr[j-h];
					arr[j-h]=temp;
				}
			}
			h=h/3; 
		}
		return arr;
	}
	
 //En el controller llamo la info de los viajes
	public Viaje[] viajesMasPromMes (int numviajes, int mes)
	{
		Viaje[]totalviajesmes=ArregloPorMes(mes);
		if(totalviajesmes.length==0)
		{
			System.out.println("No hay viajes en este mes");
			return null;
		}
		else
		{
		Viaje[] devolv= new Viaje[numviajes];
		int cont2=0;
		while(cont2<numviajes)
		{
			devolv[cont2]=totalviajesmes[cont2];
			cont2++;
		}
		return devolv;
		}
	}

	public Viaje[] ArregloPorMes(int mes)
	{
		Node<Viaje> viajenodo= colames.darPrimer();
		Cola<Viaje> viajes= new Cola<Viaje>();
		while(viajenodo!=null)
		{
			Viaje viajee=(Viaje) viajenodo.darDato();
			if(viajee.darInfo()==mes)
			{
				viajes.enqueue(viajee);
			}
			viajenodo=viajenodo.darSiguiente();
		}
		Viaje[]devolver= new Viaje[viajes.darTamano()];
		Node<Viaje> nodoo= viajes.darPrimer();
		int cont=0;
		while(nodoo!=null)
		{
			devolver[cont++]=(Viaje) nodoo.darDato();
			nodoo=nodoo.darSiguiente();
		}
		shellsort(devolver);
		return devolver;
	}
	
	public String compararTiemposmes(int mes, int idZona, int mayorZona, int menorZona)
	{
		return "";
	}
	
	//En el controller llamo la desviaci�n y el promedio de cada viaje
	public Cola<Viaje> consultarPorDiayZona(int dia, int origen, int destino)
	{
return null;
	}
	
	 //En el controller llamo la info de los viajes
	public Cola<Viaje> viajesMasPromDia(int dia, int numviajes)
	{
		return null;
	}
	
	public String compararTiemposdia(int dia, int idZona, int mayorZona, int menorZona)
	{
		return "";
	}

	 //En el controller llamo la info de los viajes
	public Cola<Viaje> viajesFranjaHoraria(int horainicial, int horafinal)
	{
		Node<Viaje> nodo= colahoras.darPrimer();
		Cola<Viaje> colish=new Cola<Viaje>();
		while(nodo!=null)
		{
			if(nodo.darDato().darInfo()>=horainicial && nodo.darDato().darInfo()<=horafinal)
			{
				colish.enqueue(nodo.darDato());
			}
			nodo=nodo.darSiguiente();
		}
		return colish;
	}
	
	 //En el controller llamo la info de los viajes
	public Viaje[] viajesMasPromHora(int hora, int numviajes)
	{
		Viaje[]totalviajeshora=ArregloPorHora(hora);
		if(totalviajeshora.length==0)
		{
			System.out.println("No hay viajes en este mes");
			return null;
		}
		else
		{
		Viaje[] devolv= new Viaje[numviajes];
		int cont2=0;
		while(cont2<numviajes)
		{
			devolv[cont2]=totalviajeshora[cont2];
			cont2++;
		}
		return devolv;
		}
	}
	
	private Viaje[] ArregloPorHora(int hora) {
		// TODO Auto-generated method stub
		Node<Viaje> viajenodo= colahoras.darPrimer();
		Cola<Viaje> viajes= new Cola<Viaje>();
		while(viajenodo!=null)
		{
			Viaje viajee=(Viaje) viajenodo.darDato();
			if(viajee.darInfo()==hora)
			{
				viajes.enqueue(viajee);
			}
			viajenodo=viajenodo.darSiguiente();
		}
		Viaje[]devolver= new Viaje[viajes.darTamano()];
		Node<Viaje> nodoo= viajes.darPrimer();
		int cont=0;
		while(nodoo!=null)
		{
			devolver[cont++]=(Viaje) nodoo.darDato();
			nodoo=nodoo.darSiguiente();
		}
		shellsort(devolver);
		return devolver;
	}
	
	public ArrayList<String> comparacionZona(int zonaP, int zonaMe, int zonaMa, int mes)
	{
		Node<Viaje> temporal= colames.darPrimer();
		Cola<Viaje> arreglo= new Cola<Viaje>();
		while(temporal!=null)
		{
			if(temporal.darDato().darInfo()==mes)
			{
				arreglo.enqueue(temporal.darDato());
			}
			temporal=temporal.darSiguiente();
		}
		
		Viaje arr[]= new Viaje[arreglo.darTamano()];
		Node<Viaje> nod = arreglo.darPrimer();
		int cont=0;
		while(nod!=null)
		{
			arr[cont]=nod.darDato();
			cont++;
			nod= nod.darSiguiente();
		}
		ArrayList<String> r = new ArrayList<String>();
		return r;
		
	}
	
	
	// PARTE B
	
	public Viaje[] consultarNViajesDia( int N, int dia)
	{
		Node<Viaje> temporal=colasemana.darPrimer();
		Cola<Viaje> arreglo= new Cola<Viaje>();
		while(temporal!=null)
		{
			if(temporal.darDato().darInfo()==dia)
			{
				arreglo.enqueue(temporal.darDato());
			}
			temporal=temporal.darSiguiente();
		}
		Viaje arr[]= new Viaje[arreglo.darTamano()];
		Node<Viaje> nod = arreglo.darPrimer();
		int cont=0;
		while(nod!=null)
		{
			arr[cont]=nod.darDato();
			cont++;
			nod= nod.darSiguiente();
		}
		
		shellsort(arr);
		Viaje[] ultimo = new Viaje[N];
		for(int i = 0; i<N; i++)
		{
			ultimo[i] = arr[i];
		}
		
		return ultimo;
	}
	
	public Cola<Viaje> consultarPorZonaDia(int zonaMenor, int zonaMayor, int dia)
	{
		Node<Viaje> temporal=colasemana.darPrimer();
		Cola<Viaje> arreglo= new Cola<Viaje>();
		while(temporal!=null)
		{
			if(temporal.darDato().darInfo()==dia && temporal.darDato().darSourceid()>=zonaMenor && temporal.darDato().darDstid()<=zonaMayor)
			{
				arreglo.enqueue(temporal.darDato());
			}
			temporal=temporal.darSiguiente();
		}
		
		Viaje arr[]= new Viaje[arreglo.darTamano()];
		Node<Viaje> nod = arreglo.darPrimer();
		int cont=0;
		while(nod!=null)
		{
			arr[cont]=nod.darDato();
			cont++;
			nod= nod.darSiguiente();
		}
		return arreglo;
	}
	
	public ArrayList<String> consultarPorZona(int zona, int zonaMenor, int zonaMayor, int dia)
	{
		Node<Viaje> temporal= colasemana.darPrimer();
		Cola<Viaje> arreglo= new Cola<Viaje>();
		while(temporal!=null)
		{
			if(temporal.darDato().darInfo()==dia)
			{
				arreglo.enqueue(temporal.darDato());
			}
			temporal=temporal.darSiguiente();
		}
		
		Viaje arr[]= new Viaje[arreglo.darTamano()];
		Node<Viaje> nod = arreglo.darPrimer();
		ArrayList<String> lista = new ArrayList<String>();
		while(nod!=null)
		{
			if(nod.darDato().darSourceid()==zona && nod.darDato().darDstid()>=zonaMenor &&  nod.darDato().darDstid()<=zonaMayor)
			{
				String conc = nod.darDato().darMean_travel_time() + " de " + zona + " a " + nod.darDato().darDstid() + " VS ";
				Node<Viaje> nodo2 = arreglo.darPrimer();
				boolean encontro = false;
				while(nodo2!=null)
				{
					if(nodo2.darDato().darSourceid()==nod.darDato().darDstid() && nodo2.darDato().darDstid()==zona)
					{
						encontro = true;
						conc = conc + nodo2.darDato().darMean_travel_time() + " de " + nodo2.darDato().darSourceid() + " a " + nodo2.darDato().darDstid();
					}
					
					nodo2 = nodo2.darSiguiente();
				}
				if(encontro = false)
				{
					conc = conc + " No hay viajes de " + nod.darDato().darDstid() + " a " + nod.darDato().darSourceid();
				}
				lista.add(conc);
			}
			
			nod= nod.darSiguiente();
		}
		return lista;
	}
	
	
	//PARTE GRUPAL
	
	
	public void graficaASCII(int zonaOrigen, int zonaDestino)
	{
		System.out.println("Aproximaci�n en minutos de viajes entre zona origen y zona destino.");
		System.out.println("Trimestre X del 2018 detallado por cada hora del d�a");
		System.out.println("Zona Origen: " + zonaOrigen);
		System.out.println("Zona Destino: "+zonaDestino);
		System.out.println("Hora| # de minutos");
		
		Node<Viaje> temporal=colahoras.darPrimer();
		Cola<Viaje> arreglo= new Cola<Viaje>();
		while(temporal!=null)
		{
			if(temporal.darDato().darSourceid()== zonaOrigen && temporal.darDato().darDstid()==zonaDestino)
			{
				arreglo.enqueue(temporal.darDato());
			}
			temporal=temporal.darSiguiente();
		}
		Viaje arr[]= new Viaje[arreglo.darTamano()];
		Node<Viaje> nod = arreglo.darPrimer();
		
		for(int i=0; i<24;i++)
		{
			double suma = 0;
			int cont = 0;
			while(nod!=null)
			{
				if(nod.darDato().darInfo()==i)
				{
					suma += nod.darDato().darMean_travel_time();
					cont++;
				}
				nod= nod.darSiguiente();
			}
			if(cont == 0)
			{
				System.out.println( i + " | hora sin viajes");
			}
			else
			{
				double promedio = suma / cont;
				double min = promedio/60;
				long valor = Math.round(min);
				String conca = "";
				for(int o=0;o<valor;o++)
				{
					conca = conca + "*";
				}
				System.out.println( i + " | " + conca);
			}
		}
		
	}
	
	
}




