package view;

import model.logic.MVCModelo;

public class MVCView 
{
	private MVCModelo modelo;
	    /**
	     * Metodo constructor
	     */
	    public MVCView()
	    {
	    	modelo = new MVCModelo();
	    }
	    
		public void printMenu()
		{
			System.out.println("1. Cargar archivo de UBER");
			System.out.println("2. 1A- Consultar el tiempo promedio de viaje y su desviaci�n est�ndar de los viajes entre una zona de origen y una zona destino para un mes dado. ");
			System.out.println("3. 2A- Consultar la informaci�n de los N viajes con mayor tiempo promedio para un mes dado. ");
			System.out.println("4. 3A� Comparar los tiempos promedios de los viajes para una zona dada contra cada zona X en un rango de zonas dado");
			System.out.println("5. 1B- Consultar el tiempo promedio de viaje y su desviaci�n est�ndar de los viajes entre una zona de origen y una zona destino para un d�a dado de la semana.");
			System.out.println("6. 2B- Consultar la informaci�n de los N viajes con mayor tiempo promedio para un d�a dado.");
			System.out.println("7. 3B- Comparar los tiempos promedios de los viajes para una zona dada contra cada zona X en un rango de zonas dado");
			System.out.println("8. 1C- Consultar los viajes entre una zona de origen y una zona destino en una franja horaria (hora inicial � hora final) dada.");
			System.out.println("9. 2C- Consultar la informaci�n de los N viajes con mayor tiempo promedio para una hora dada. ");
			System.out.println("10. 3C- Generar una gr�fica ASCII que muestre el tiempo promedio de los viajes entre una zona origen y una zona destino para cada hora del d�a. ");
			System.out.println("11. Exit");
			System.out.println("Dar el numero de opcion a resolver, luego oprimir tecla Return: (e.g., 1):");
		}

		public void printMessage(String mensaje) {

			System.out.println(mensaje);
		}		
		
		public void printModelo(MVCModelo modelo)
		{
			// TODO implementar
		}


}
