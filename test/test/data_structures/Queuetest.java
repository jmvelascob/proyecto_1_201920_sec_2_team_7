package test.data_structures;

import static org.junit.Assert.assertEquals;


import org.junit.Before;
import org.junit.Test;

import model.data_structures.Cola;

public class Queuetest {

	private Cola <String> queue;
	
	private String uno="uno";
	
	private String dos = "dos";
	
	private String tres = "tres";
	
	private String cua = "cuatro";
	
	private String cin = "cinco";
	
	@Before
	public void SetUp1()
	{
		queue= new Cola <String>();
		queue.enqueue(uno);
		queue.enqueue(dos);
		queue.enqueue(tres);
		queue.enqueue(cua);
		queue.enqueue(cin);
	}
	
	@Before
	public void setUpVacio()
	{
		queue= new Cola<String>();
	}
	
	@Test
	public void tama�o()
	{
		SetUp1();
		assertEquals("El tama�o es 5",5, queue.darTamano() );
		setUpVacio();
		assertEquals("El tama�o es 0",0, queue.darTamano() );
		
	}

	@Test
	public void dequeue()
	{
		SetUp1();
		
		queue.dequeue();
		
		assertEquals("El tama�o es 4",4, queue.darTamano() );
		
	}
	
	@Test
	public void esVacio()
	{
		SetUp1();
		assertEquals("No est� vacio", false, queue.estaVacia());
		setUpVacio();
		assertEquals("Est� vacio", true, queue.estaVacia());
	}
	
	@Test
	public void primero()
	{
		SetUp1();
		assertEquals("El primero es uno", uno, queue.darPrimer().darDato());
		
		queue.dequeue();

		assertEquals("El primero ahora es 2",dos, queue.darPrimer().darDato() );


	}
	
	@Test
	public void enqueue()
	{
		SetUp1();
		queue.enqueue("seis");
		assertEquals("Tama�o es 6",6, queue.darTamano() );
	}
}
	